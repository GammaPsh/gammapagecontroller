﻿using UnityEngine;
using System.Collections;

namespace Gamma.PageHandling{
	public interface IView {

		Transform transform {get;}
		Rect viewRect {get;}

		void Dispose();
	}
}
