﻿using UnityEngine;
using System.Collections;

namespace Gamma.PageHandling.UGUI {
	public class UView : MonoBehaviour, IView {

		public RectTransform rectTransform {
			get {
				return (transform as RectTransform);
			}
		}

		#region IView implementation

		public Rect viewRect {
			get {
				return rectTransform.rect;
			}
		}

		public virtual void Dispose()
		{
			if(this != null && this.gameObject != null)
				Destroy(gameObject);
		}

		#endregion



	}
}
