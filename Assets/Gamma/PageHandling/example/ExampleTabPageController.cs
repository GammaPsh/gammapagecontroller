﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Gamma.PageHandling;

public class ExampleTabPageController : TabController {

	public string nextPageId;

	#region ButtonInputListener
	public void NextButtonPressed()
	{
		if(pageId == "page5")
			navigationController.PopPage("page1");
		else
			navigationController.PushPage(nextPageId);
	}

	public void BackButtonPressed()
	{
		if(this.pageId == "page1"){
			navigationController.PushPage("page5");
			navigationController.InsertIntoStack("page4", 1);
			navigationController.InsertIntoStack("page3", 1);
			navigationController.InsertIntoStack("page2", 1);
		}else{
			navigationController.PopPage();
		}
	}

	public void ResetButtonPressed()
	{
		if(pageId == "overlay"){
			navigationController.PushOverlayPage("overlay2");
		}else{
			navigationController.PushOverlayPage("overlay");
		}
	}
	#endregion
}
