﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Gamma.PageHandling;

public class ExamplePageController : PageController {

	public string nextPageId;

	public Text pageTitleLabel;

	protected override void OnPageEnable ()
	{
		base.OnPageEnable ();

		pageTitleLabel.text = string.Format("This is page: {0}\nOpened at; {1}", pageId, System.DateTime.Now.ToLongTimeString());
	}

	#region ButtonInputListener
	public void NextButtonPressed()
	{
		if(pageId == "page5")
			navigationController.PopPage("page1");
		else
			navigationController.PushPage(nextPageId);
	}

	public void BackButtonPressed()
	{
		if(this.pageId == "page1"){
			navigationController.PushPage("page5");
			navigationController.InsertIntoStack("page4", 1);
			navigationController.InsertIntoStack("page3", 1);
			navigationController.InsertIntoStack("page2", 1);
		}else{
			navigationController.PopPage();
		}
	}

	public void ResetButtonPressed()
	{
		if(pageId == "overlay"){
			navigationController.PushOverlayPage("overlay2");
		}else{
			navigationController.PushOverlayPage("overlay");
		}
	}
	#endregion
}
