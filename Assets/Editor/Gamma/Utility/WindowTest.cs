﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

class SimpleEnumMaskUsage : EditorWindow {

	[Flags]
	public enum PageTweenType {
		GetFocus = 16,
		None = 0,
		PushEnable = 1,
		PushDisable = 2,
		PopEnable = 4,
		LoseFocus = 32,
		PopDisable = 8,
	}

	[MenuItem("Examples/Mask Field Usage")]
	public static void Init() {
		var window = GetWindow<SimpleEnumMaskUsage>();
		window.Show();
	}

	int staticFlagMask  = 0;
	void OnGUI() {
		
		staticFlagMask = staticFlagMask == 0 ? 1 : staticFlagMask << 1;
		Enum targetEnum = EditorGUILayout.EnumMaskField ( "Static Flags", (PageTweenType)staticFlagMask );
		staticFlagMask = ((int) Convert.ChangeType(targetEnum, targetEnum.GetType()) >> 1);
		EditorGUILayout.LabelField("PageTweenType Value", ((int)staticFlagMask).ToString());
	}
}
