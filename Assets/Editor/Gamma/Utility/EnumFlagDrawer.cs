﻿using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(EnumFlagAttribute))]
public class EnumFlagDrawer : PropertyDrawer {
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		Enum targetEnum = GetBaseProperty<Enum>(property);
		int targetEnumValue = Convert.ToInt32(targetEnum);

		foreach(var en in Enum.GetValues(targetEnum.GetType())){
			if((int)en == 0){
				//Convert to Enum Without 0 Flag for Unity GUI
				targetEnumValue = targetEnumValue == 0 ? 1 : targetEnumValue << 1;
				break;
			}
		}

		targetEnum = (Enum)Enum.ToObject(targetEnum.GetType(), targetEnumValue);

		EditorGUI.BeginProperty(position, label, property);

		int newEnumValue = Convert.ToInt32(EditorGUI.EnumMaskField(position, property.name, targetEnum));

		newEnumValue = newEnumValue >> 1;
		property.intValue = newEnumValue;

		EditorGUI.EndProperty();
	}

	static T GetBaseProperty<T>(SerializedProperty prop)
	{
		// Separate the steps it takes to get to this property
		string[] separatedPaths = prop.propertyPath.Split('.');

		// Go down to the root of this serialized property
		System.Object reflectionTarget = prop.serializedObject.targetObject as object;
		// Walk down the path to get the target object
		foreach (var path in separatedPaths)
		{
			FieldInfo fieldInfo = reflectionTarget.GetType().GetField(path);
			reflectionTarget = fieldInfo.GetValue(reflectionTarget);
		}
		return (T) reflectionTarget;
	}
}